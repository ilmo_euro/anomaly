shader_type spatial;

uniform float subdivisionU;
uniform float subdivisionV;
uniform float thickness;

void fragment() {
	float x = mod(UV.x * subdivisionU, 1.0);
	float z = mod(UV.y * subdivisionV, 1.0);
	ALPHA = (x < thickness || z < thickness) ? 1.0 : 0.0;
}