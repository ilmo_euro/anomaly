extends Label

export(NodePath) var p1_path
export(NodePath) var p2_path

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var p1 = get_node(p1_path).hit_count
	var p2 = get_node(p2_path).hit_count
	text = "P1: %d P2: %d" % [p1, p2]
