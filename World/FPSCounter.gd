extends Label

export(int) var averaging = 10

var samples = []

func _process(delta):
	samples.push_back(delta)
	while samples.size() > averaging:
		samples.pop_front()

	var sum = 0
	for sample in samples:
		sum += sample

	var avg = sum / averaging
	text = "%f FPS" % (1/avg)
