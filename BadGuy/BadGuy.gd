extends KinematicBody

var is_pointed = false
export var kill_action_1 = ""
export var kill_action_2 = ""
var dx = 0
var dy = 0
var counter = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func on_pointer_enter(source):
	is_pointed = true

func on_pointer_exit(source):
	is_pointed = false

func _physics_process(delta):
	if is_pointed and \
			(Input.is_action_pressed(kill_action_1) or \
			Input.is_action_pressed(kill_action_2)):
		get_parent().remove_child(self)
	if counter > 0:
		move_and_collide(Vector3(dx, 0, dy))
		counter -= 1
	else:
		counter = 100
		dx = rand_range(-0.1, 0.1)
		dy = rand_range(-0.1, 0.1)

