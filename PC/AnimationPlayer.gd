extends AnimationPlayer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
var old_pos = Vector3(0,0,0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var pos = owner.to_global(Vector3(0,0,0))
	if pos.distance_squared_to(old_pos) > 0:
		play("walk-loop", -1, 5.0)
	else:
		play("idle-loop")
	old_pos = pos