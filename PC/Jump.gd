extends RayCast

export var action: String = "kb_jump"
export var force: float = 20.0
export var gravity: float = 30.0

var state
var pc: KinematicBody
var speed: float = 0

func _ready():
	pc = get_owner()
	assert(pc is KinematicBody)
	state = process_coroutine()

func _physics_process(delta):
	if state != null:
		state = state.resume(delta)

func fall(delta):
	pc.move_and_collide(Vector3(0, speed * delta, 0))
	speed -= delta*gravity

func process_coroutine():
	var delta = yield()
	while true:
		speed = 0
		while not (is_colliding() and Input.is_action_pressed(action)):
			delta = yield()
			if is_colliding():
				speed = 0
			else:
				fall(delta)
		speed = force
		while is_colliding():
			delta = yield()
			fall(delta)
		while not is_colliding():
			delta = yield()
			fall(delta)

# vim:ts=4:sw=4:noexpandtab