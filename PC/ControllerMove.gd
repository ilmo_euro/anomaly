extends Node

export var enabled: bool = true
export var joy_index: int = 0
export var x_speed: float = 10.0
export var y_speed: float = 15.0
export var x_axis: int = 0
export var y_axis: int = 1
export var dead_zone: float = 0.2
var pc: KinematicBody

func _ready():
	pc = get_owner()
	assert(pc is KinematicBody)

func _physics_process(delta):
	if not enabled:
		return
	var pads = Input.get_connected_joypads()
	if pads.size() <= joy_index:
		return
	var x = Input.get_joy_axis(pads[joy_index], x_axis)
	var y = Input.get_joy_axis(pads[joy_index], y_axis)
	if x*x + y*y < dead_zone:
		return
	var vec = Vector3(x_speed*delta*x, 0, y_speed*delta*y)
	vec = vec.rotated(Vector3(0, 1, 0), pc.rotation.y)
	pc.move_and_collide(vec)

# vim:ts=4:sw=4:noexpandtab