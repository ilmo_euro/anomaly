extends Node

export var enabled: bool = true
export var x_turn_speed: float = 0.75
export var y_turn_speed: float = 0.75
export var y_min: float = -1.0
export var y_max: float = 1.0
var mouse_speed = null
var camera = null
var pc: KinematicBody

func _ready():
	pc = get_owner()
	assert(pc is KinematicBody)
	var siblings = pc.get_children()
	for sibling in siblings:
		if sibling is Camera:
			camera = sibling
			break

func _input(ev):
	if ev is InputEventMouseMotion:
		if mouse_speed == null:
			mouse_speed = Vector2(0.0, 0.0)
		mouse_speed += ev.relative

func _physics_process(delta):
	if not enabled or Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		return
	if mouse_speed != null:
		pc.rotation.y += -x_turn_speed*mouse_speed.x*delta
		if camera != null:
			camera.rotate_x(-y_turn_speed*mouse_speed.y*delta)
			if camera.rotation.x < y_min:
				camera.rotation.x = y_min
			if camera.rotation.x > y_max:
				camera.rotation.x = y_max
		mouse_speed = null

# vim:ts=4:sw=4:noexpandtab