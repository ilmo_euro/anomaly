extends Node

const MouseLook = preload("res://PC/MouseLook.gd")

export var zoom_action: String = "p1_zoom"
export var zoom_fov: float = 20.0
var original_fov: float
var camera: Camera = null
var mouselook: MouseLook = null
var original_speed_x: float = 0.0
var original_speed_y: float = 0.0

func _ready():
	assert(owner is KinematicBody)
	var siblings = owner.get_children()
	for sibling in siblings:
		if sibling is Camera:
			camera = sibling
			original_fov = camera.fov
			break
	for sibling in siblings:
		if sibling is MouseLook:
			mouselook = sibling
			original_speed_x = mouselook.x_turn_speed
			original_speed_y = mouselook.y_turn_speed
			break

#warning-ignore:unused_argument
func _process(delta):
	if Input.is_action_pressed(zoom_action):
		camera.fov = zoom_fov
		mouselook.x_turn_speed = original_speed_x*0.2
		mouselook.y_turn_speed = original_speed_y*0.2
	else:
		camera.fov = original_fov
		mouselook.x_turn_speed = original_speed_x
		mouselook.y_turn_speed = original_speed_y