extends Area

export var hit_count: int = 0
export var kill_action_1: String = "p1_kb_shoot"
export var kill_action_2: String = "p2_kb_shoot"
var is_pointed: bool = false
var initial_translation: Vector3
var source: Node

# Called when the node enters the scene tree for the first time.
func _ready():
	initial_translation = owner.translation

func on_pointer_enter(source):
	is_pointed = true
	self.source = source

func on_pointer_exit(source):
	is_pointed = false
	self.source = source

func _physics_process(delta):
	if is_pointed and \
			((Input.is_action_pressed(kill_action_1) \
			and source.name == "PC2") or \
			(Input.is_action_pressed(kill_action_2) \
			and source.name == "PC")):
		hit_count += 1
		owner.translation = initial_translation
