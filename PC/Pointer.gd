extends RayCast

var pointed_object = null

func _ready():
	add_exception(get_owner())

func _physics_process(delta):
	if is_colliding():
		var collider = get_collider()
		if not collider.has_method("get_instance_id"):
			return
		if pointed_object != null \
				&& pointed_object.has_method("on_pointer_exit") \
				&& collider.get_instance_id() \
				!= pointed_object.get_instance_id():
			pointed_object.on_pointer_exit(get_owner())
		if collider.has_method("on_pointer_enter") \
				&& (pointed_object == null \
				|| pointed_object.get_instance_id() \
				!= collider.get_instance_id()):
			collider.on_pointer_enter(get_owner())
		pointed_object = collider
	else:
		if pointed_object != null \
				&& pointed_object.has_method("on_pointer_exit"):
			pointed_object.on_pointer_exit(get_owner())
		pointed_object = null