extends KinematicBody

export var x_turn_speed = 1.0
export var y_turn_speed = 0.5
export var y_min = -1.0
export var y_max = 1.0
export var forward_action = "p1_kb_forward"
export var backward_action = "p1_kb_backward"
export var strafe_left_action = "p1_kb_strafe_left"
export var strafe_right_action = "p1_kb_strafe_right"
export var up_action = "p1_kb_up"
export var down_action = "p1_kb_down"
export var left_action = "p1_kb_left"
export var right_action = "p1_kb_right"
export var speed = 15.0
export var strafe_speed = 10.0
export var joylook_index = 0
export var joylook_multiplier = 10.0
export var joylook_x_axis = 2
export var joylook_y_axis = 3
export var joylook_curve = 2
export var joylook_dead_zone = 0.1
export var joymove_multiplier = 1.0
export var joymove_x_axis = 0
export var joymove_y_axis = 1
export var joymove_dead_zone = 0.1
export var jump_action = "kb_jump"
export var jump_speed = 10.0
export var jump_gravity = 10.0
export var joy_enabled = true
export var kb_enabled = true
export var mouse_enabled = true
export var player_number = 1
export var shadow_group = "shadow"

func _ready():
	add_to_group(shadow_group, true)

func _suppress_warnings():
	x_turn_speed = 10.0
	y_turn_speed = 0.5
	y_min = 1.0
	y_max = 1.0
	forward_action = "p1_kb_forward"
	backward_action = "p1_kb_backward"
	strafe_left_action = "p1_kb_strafe_left"
	strafe_right_action = "p1_kb_strafe_right"
	up_action = "p1_kb_up"
	down_action = "p1_kb_down"
	left_action = "p1_kb_left"
	right_action = "p1_kb_right"
	speed = 1.0
	strafe_speed = 1.0
	joylook_index = 0
	joylook_multiplier = 10.0
	joylook_x_axis = 2
	joylook_y_axis = 3
	joylook_curve = 2
	joylook_dead_zone = 0.01
	joymove_multiplier = 10.0
	joymove_x_axis = 2
	joymove_y_axis = 3
	joymove_dead_zone = 0.1
	jump_action = "kb_jump"
	jump_speed = 10.0
	jump_gravity = 1.0
	joy_enabled = true
	kb_enabled = true
	mouse_enabled = true
	player_number = 1

# vim:ts=4:sw=4:noexpandtab