extends Node

export var enabled: bool = true
export var up_action: String = "p1_kb_up"
export var down_action: String = "p1_kb_down"
export var left_action: String = "p1_kb_left"
export var right_action: String = "p1_kb_right"
export var x_turn_speed: float = 10.0
export var y_turn_speed: float = 5.0
export var y_min: float = -1.0
export var y_max: float = 1.0

var camera = null
var pc

func _ready():
	pc = get_owner()
	assert(pc is KinematicBody)
	var siblings = pc.get_children()
	for sibling in siblings:
		if sibling is Camera:
			camera = sibling
			break

func _physics_process(delta):
	if not enabled:
		return
	if Input.is_action_pressed(left_action):
		pc.rotation.y += x_turn_speed*delta
	if Input.is_action_pressed(right_action):
		pc.rotation.y += -x_turn_speed*delta
	if camera != null:
		if Input.is_action_pressed(up_action):
			camera.rotate_x(y_turn_speed*delta)
		if Input.is_action_pressed(down_action):
			camera.rotate_x(-y_turn_speed*delta)
		if camera.rotation.x < y_min:
			camera.rotation.x = y_min
		if camera.rotation.x > y_max:
			camera.rotation.x = y_max

# vim:ts=4:sw=4:noexpandtab