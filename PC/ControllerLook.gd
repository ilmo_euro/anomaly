extends Node

export var enabled: bool = true
export var joy_index: int = 0
export var x_speed: float = 10.0
export var y_speed: float = 5.0
export var x_axis: int = 2
export var y_axis: int = 3
export var y_min: float = -1.0
export var y_max: float = 1.0
export var curve: float = 4.0
export var dead_zone: float = 0.1

var camera = null
var pc: KinematicBody

func _ready():
	pc = get_owner()
	assert(pc is KinematicBody)
	var siblings = pc.get_children()
	for sibling in siblings:
		if sibling is Camera:
			camera = sibling
			break

func _physics_process(delta):
	if not enabled:
		return
	var pads = Input.get_connected_joypads()
	if pads.size() <= joy_index:
		return
	var x = Input.get_joy_axis(pads[joy_index], x_axis)
	var y = Input.get_joy_axis(pads[joy_index], y_axis)
	if x*x + y*y > dead_zone:
		x = sign(x) * pow(abs(x), curve)
		y = sign(y) * pow(abs(y), curve)
		pc.rotation.y += -x_speed*x*delta
		if camera != null:
			camera.rotate_x(-y_speed*y*delta)
			if camera.rotation.x < y_min:
				camera.rotation.x = y_min
			if camera.rotation.x > y_max:
				camera.rotation.x = y_max

# vim:ts=4:sw=4:noexpandtab