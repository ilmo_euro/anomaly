extends Node

export var enabled: bool = true
export var forward_action: String = "p1_kb_forward"
export var backward_action: String = "p1_kb_backward"
export var strafe_left_action: String = "p1_kb_strafe_left"
export var strafe_right_action: String = "p1_kb_strafe_right"
export var x_speed: float = 10.0
export var y_speed: float = 15.0

var pc: KinematicBody

func _ready():
	pc = get_owner()
	assert(pc is KinematicBody)

func _physics_process(delta):
	if not enabled:
		return
	var vec = Vector3(0.0,0.0,0.0)
	if Input.is_action_pressed(forward_action):
		vec += Vector3(0, 0, -y_speed*delta)
	if Input.is_action_pressed(backward_action):
		vec += Vector3(0, 0, y_speed*delta)
	if Input.is_action_pressed(strafe_left_action):
		vec += Vector3(-x_speed*delta, 0, 0)
	if Input.is_action_pressed(strafe_right_action):
		vec += Vector3(x_speed*delta, 0, 0)
	vec = vec.rotated(Vector3(0, 1, 0), pc.rotation.y)
	pc.move_and_collide(vec)

# vim:ts=4:sw=4:noexpandtab